#!/usr/bin/env python
# vim: set fileencoding=utf-8 tabstop=4 shiftwidth=4 expandtab smartindent:
u"""

Backup Delicious
----------------

Backs up the bookmarks stored on Delicious.com into an XML file.

.. :Authors:
       Aurélien Bompard <aurelien@bompard.org> <http://aurelien.bompard.org>

.. :License:
       GNU GPL v3 or later
"""

import os
import sys
import optparse
import urllib2
from xml.etree import ElementTree as etree
import datetime

backup = os.path.expanduser("~/tmp/bookmarks.xml")

def setup_url_opener(username, password):
    auth_handler = urllib2.HTTPBasicAuthHandler()
    auth_handler.add_password(realm="del.icio.us API",
                              uri='https://api.del.icio.us/v1/posts/',
                              user=username, passwd=password)
    opener = urllib2.build_opener(auth_handler)
    opener.addheaders = [('User-agent', 'Mozilla/5.0')]
    urllib2.install_opener(opener)

def get_update_time():
    update_page = urllib2.urlopen("https://api.del.icio.us/v1/posts/update")
    update = etree.parse(update_page)
    update_page.close()
    update_time = datetime.datetime.strptime(
                    update.getroot().get("time"),
                    "%Y-%m-%dT%H:%M:%SZ")
    return update_time

def get_backup_time(backup_file):
    if not os.path.exists(backup_file):
        return datetime.datetime(1970,1,1,0,0,0)
    statinfo = os.stat(backup_file)
    return datetime.datetime.fromtimestamp(statinfo.st_mtime)

def backup_delicious(backup_file):
    backup_file = open(backup_file, "w")
    bookmarks_page = urllib2.urlopen("https://api.del.icio.us/v1/posts/all")
    backup_file.write(bookmarks_page.read())
    backup_file.close()
    bookmarks_page.close()

def parse_opts():
    parser = optparse.OptionParser()
    parser.add_option("-u", "--username", dest="username",
                      help="User name on Delicious.com")
    parser.add_option("-p", "--password", dest="password",
                      help="Password on Delicious.com")
    parser.add_option("-f", "--file", dest="backup",
                      help="File to store the backup into")
    options, args = parser.parse_args()
    if len(args):
        parser.error("No arguments allowed.")
    if not options.username or not options.password:
        parser.error("You must provide credentials to Delicious.com.")
    if not options.backup:
        parser.error("You must provide a file to backup into.")
    return options

def main():
    options = parse_opts()
    setup_url_opener(options.username, options.password)
    backup_file = os.path.expanduser(options.backup)
    update_time = get_update_time()
    backup_time = get_backup_time(backup_file)

    if update_time > backup_time:
        print "Bookmarks have been updated, backing up...   ",
        sys.stdout.flush()
        backup_delicious(backup_file)
        print "done."
    else:
        print "No update available, backup cancelled."

if __name__ == "__main__":
    main()
