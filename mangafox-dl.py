#!/usr/bin/env python
# -*- coding: utf-8 -*-
u"""

Mangafox-DL
-----------

Downloads mangas from MangaFox_ to the local drive as images, and optionnaly
creates a PDF with these images.

.. _MangaFox: http://www.mangafox.com

The PDF is in A4-size, so it's best printed in "leaflet" mode.

Dependencies:

- `BeautifulSoup <http://www.crummy.com/software/BeautifulSoup>`_
- `ReportLab <http://www.reportlab.com/software/opensource>`_
- `PIL <http://www.pythonware.com/products/pil>`_


.. :Authors:
       Aurélien Bompard <aurelien@bompard.org> <http://aurelien.bompard.org>

.. :License:
       GNU GPL v3 or later

"""

import os
import sys
import urllib
import urllib2
import urlparse
import re
from optparse import OptionParser

from BeautifulSoup import BeautifulSoup
from reportlab.pdfgen import canvas
from reportlab.lib.pagesizes import A4
import PIL.Image

BASE_URL = "http://www.mangafox.com/manga"


class MangaError(Exception): pass

class Manga(object):
    """
    This is the main Manga object, which contains Chapter instances. It's
    mainly a container object.
    """

    def __init__(self, name):
        self.name = name
        self.url = "%s/%s/" % (BASE_URL, name)
        self.subdir = os.path.join(options.output, name)
        self._chapters = None

    def _get_chapters(self):
        if self._chapters is not None:
            return self._chapters
        main_index = os.path.join(self.subdir, "index.txt")
        if os.path.exists(main_index):
            chapurls = self._get_chapter_urls_index()
        else:
            chapurls = self._get_chapter_urls_web()
        self._chapters = []
        for chapter_url in chapurls:
            chapter = Chapter(self, chapter_url)
            self._chapters.append(chapter)
        self.filter_chapters(options.volume, options.chapter)
        self._chapters.sort()
        return self._chapters
    chapters = property(_get_chapters)

    def _get_chapter_urls_index(self):
        urllist = []
        main_index = os.path.join(self.subdir, "index.txt")
        chapfile = open(main_index)
        for line in chapfile:
            volnum, chapnum = line.strip().split(" ")
            urllist.append("v%s/c%s" % (volnum, chapnum))
        return urllist

    def _get_chapter_urls_web(self):
        main_page = BeautifulSoup(urllib2.urlopen(self.url))
        listing = main_page.find("table", attrs={"id":"listing"})
        if not listing:
            raise MangaError("Error reading the web page")
        urllist = []
        for chaplink in listing.findAll("a", attrs={"class": "chico"}):
            urllist.append(urlparse.urljoin(self.url, chaplink["href"]))
        return urllist

    def filter_chapters(self, volnum=None, chapnum=None):
        for chapter in self._chapters[:]:
            if volnum and chapter.volnum != volnum:
                if options.debug:
                    print "found volume %s but you asked for %s, skipping." \
                            % (chapter.volnum, volnum)
                self._chapters.remove(chapter)
                continue
            if chapnum and chapter.chapnum != chapnum:
                if options.debug:
                    print "found chapter %s but you asked for %s, skipping." \
                            % (chapter.chapnum, chapnum)
                self._chapters.remove(chapter)
                continue

    def download(self):
        if len(self.chapters) == 0:
            print "No chapter matching your selection !"
            return
        for chapter in self.chapters:
            chapter.download()

    def make_pdf(self):
        if len(self.chapters) == 0:
            print "No chapter matching your selection !"
            return
        print "Building PDF..."
        self.page_size = A4
        self.margin = 0
        #self.move_down = 35
        self.move_down = 0
        if options.volume:
            if options.chapter:
                pdf_filename = "%s-%s-%s.pdf" % (self.name.title(),
                                    options.volume, options.chapter)
                pdf_title = "%s - vol. %s, chap. %s" % (self.name.title(),
                                    options.volume, options.chapter)
            else:
                pdf_filename = "%s-%s.pdf" % (self.name.title(), options.volume)
                pdf_title = "%s - vol. %s" % (self.name.title(), options.volume)
        else:
            pdf_filename = "%s.pdf" % self.name.title()
            pdf_title = self.name.title()
        self.pdf = canvas.Canvas(os.path.join(self.subdir, pdf_filename),
                                 pagesize=self.page_size)
        self.pdf.setTitle(pdf_title)
        for chapter in self.chapters:
            chapter.add_to_pdf(self.pdf)
        self.pdf.save()


class Chapter(object):
    """
    This is a chapter objects, contained in a Manga object. It "knows" how to
    download itself from MangaFox and how to append itself to the PDF (if
    requested)

    We use an ``index.txt`` file to store the order of the images. This is an
    optimisation to allow generating a PDF when the images are already
    downloaded, without having to go back to MangaFox and parse the webpages.
    """

    def __init__(self, manga, url):
        self.manga = manga
        self.url = url
        _suburl = self.url.replace(self.manga.url, "")
        self.volnum = _suburl.split("/")[0][1:]
        self.chapnum = _suburl.split("/")[1][1:]
        self.subdir = os.path.join(self.manga.subdir, self.volnum, self.chapnum)
        self.title = "Volume %s, chapter %s" % (self.volnum, self.chapnum)
        self._pagenums = None
        self._pages = {}

    def __repr__(self):
        return "<Chapter %s, volume %s of %s>" \
                    % (self.chapnum, self.volnum, self.manga.name)

    def __cmp__(self, other):
        if not isinstance(other, Chapter):
            raise ValueError("Only comparasion between Chapter instances is supported")
        if self.volnum != other.volnum:
            return cmp(self.volnum, other.volnum)
        return cmp(self.chapnum, other.chapnum)

    def _get_pages(self):
        if self._pages:
            return self._pages
        index_path = os.path.join(self.subdir, "index.txt")
        if os.path.exists(index_path): # index found, use it
            self._pages = {}
            index = open(index_path, "r")
            for line in index:
                key, value = line.strip().split(" ")
                self._pages[key] = value
            index.close()
        return self._pages
    pages = property(_get_pages)

    def _get_pagenums(self):
        if self._pagenums is not None:
            return self._pagenums
        index_path = os.path.join(self.subdir, "index.txt")
        if os.path.exists(index_path): # index found, use it
            self._pagenums = []
            index = open(index_path, "r")
            for line in index:
                self._pagenums.append(line.split(" ")[0])
            index.close()
        else: # index not found, parse the webpage
            chapter_page = BeautifulSoup(urllib2.urlopen(self.url))
            pages_select = chapter_page.find("div", attrs={"class": "right middle"}) \
                                         .find("select").findAll("option")
            self._pagenums = [ o["value"] for o in pages_select ]
        return self._pagenums
    pagenums = property(_get_pagenums)

    def download(self):
        if not os.path.exists(self.subdir):
            os.makedirs(self.subdir)
        for pagenum in self.pagenums:
            sys.stdout.write("\r%s, page %s / %s" 
                             % (self.title, pagenum, len(self.pagenums)))
            sys.stdout.flush()
            if pagenum in self.pages.keys():
                local_filename = os.path.join(self.subdir, self.pages[pagenum])
                if os.path.exists(local_filename):
                    continue # already downloaded
            page_url = "%s%s.html" % (self.url, pagenum)
            page = BeautifulSoup(urllib2.urlopen(page_url))
            img = page.find("img", attrs={"id": "image"})
            img_url = img["src"]
            filename = os.path.basename(img_url)
            local_filename = os.path.join(self.subdir, filename)
            if not os.path.exists(local_filename):
                urllib.urlretrieve(img_url, local_filename)
            index = open(os.path.join(self.subdir, "index.txt"), "a")
            index.write("%s %s\n" % (pagenum, filename))
            index.close()
            self.pages[pagenum] = filename
        print

    def add_to_pdf(self, pdf):
        outlinekey = str("v%s-c%s" % (self.volnum, self.chapnum))
        pdf.bookmarkPage(outlinekey)
        pdf.addOutlineEntry(self.title, outlinekey)
        for pagenum in self.pagenums:
            local_filename = os.path.join(self.subdir, self.pages[pagenum])
            try:
                self.add_image_to_pdf(local_filename, pdf)
            except IOError, e:
                print >>sys.stderr, "Error adding file %s: %s" \
                                    % (local_filename, e)
                continue
            pdf.showPage()
        #if len(self.pages) % 2 != 0:
        #    pdf.showPage()

    def add_image_to_pdf(self, filename, pdf):
        img_pil = PIL.Image.open(filename)
        img_height = self.manga.page_size[1] - 2 * self.manga.margin \
                     + self.manga.move_down
        img_width = int(img_pil.size[0] * img_height / float(img_pil.size[1]))
        img_y = self.manga.margin - self.manga.move_down
        img_x = (self.manga.page_size[0] - img_width) / 2
        if img_x < self.manga.margin: # Damn ! resize by the other dimension
            img_width = self.manga.page_size[0] - 2 * self.manga.margin
            img_height = int(img_pil.size[1] * img_width / float(img_pil.size[0]))
            img_x = self.manga.margin
            img_y = (self.manga.page_size[1] - img_height \
                        + self.manga.move_down) / 2
        #pdf.drawInlineImage(filename, img_x, img_y, img_width, img_height) # raises AttributeError
        pdf.drawInlineImage(img_pil, img_x, img_y, img_width, img_height)


def parse_opts():
    usage = "%prog [-o directory] [-v volume] [-c chapter] [-p] [-d] name"
    parser = OptionParser(usage=usage)
    parser.add_option("-o", "--output", dest="output", default=".",
                      help="Download in this directory (default: current dir)")
    parser.add_option("-v", "--volume", dest="volume",
                      help="Only download this volume")
    parser.add_option("-c", "--chapter", dest="chapter",
                      help="Only download this chapter")
    parser.add_option("-p", "--pdf", dest="pdf", action="store_true",
                      help="Make a PDF")
    parser.add_option("-d", "--debug", dest="debug", action="store_true",
                      help="Print more stuff")
    options, args = parser.parse_args()
    if len(args) != 1:
        parser.error("You must give a manga name, as found in the Mangafox URL")
    return options, args[0]


def main():
    global options, subdir
    options, manga_name = parse_opts()
    manga = Manga(manga_name)
    try:
        manga.download()
    except MangaError, e:
        print >>sys.stderr, e
        sys.exit(1)
    except KeyboardInterrupt:
        print >>sys.stderr, "Aborted by user"
        sys.exit(0)
    if options.pdf:
        manga.make_pdf()


if __name__ == "__main__":
    main()
