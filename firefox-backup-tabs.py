#!/usr/bin/env python
# vim: set fileencoding=utf-8 tabstop=4 shiftwidth=4 expandtab smartindent:

u"""

Firefox Backup Tabs
-------------------

Backs up the list of open Firefox tabs to a git repository.

.. :Authors:
      Aurélien Bompard <aurelien@bompard.org> <http://aurelien.bompard.org>

.. :License:
      GNU GPL v3 or later
"""


import os
import optparse
import json
import shutil
from subprocess import Popen, PIPE, STDOUT, call
from pprint import pprint


SESSIONSTORE_PATH = os.path.expanduser("~/.mozilla/firefox/default.pb3/sessionstore.js")
MONTHS_TO_KEEP = 1


def parse_sessionstore(path):
    with open(path) as store_file:
        store = json.load(store_file)
    return [ t["entries"][-1]["url"] for t in store["windows"][0]["tabs"] ]

def run(*args, **kwargs):
    global opts
    if opts.verbose:
        print " ".join(args[0])
    return call(*args, **kwargs)

def main():
    global opts
    parser = optparse.OptionParser(usage="%prog [-r repo] action")
    parser.add_option("-r", "--repo", help="Repository path")
    parser.add_option("-v", "--verbose", action="store_true", help="Show commands")
    opts, args = parser.parse_args()
    if len(args) != 1:
        parser.error("Missing action")

    action = args[0]

    if action == "store":
        if opts.repo is None:
            parser.error("Missing repository path")
        opts.repo = os.path.expanduser(opts.repo)
        if not os.path.exists(opts.repo):
            parser.error("The repository must exist")
        os.chdir(opts.repo)
        if not os.path.exists(os.path.join(opts.repo, ".git")):
            run(["git", "init", "-q"])
        if os.path.exists(os.path.join(opts.repo, ".git", "rebase-apply")):
            run(["git", "rebase", "--abort"])
            run(["git", "checkout", "master"])
            run(["git", "branch", "-D", "temp"])
        shutil.copy(SESSIONSTORE_PATH, "sessionstore.js")
        run(["git", "add", "sessionstore.js"])
        tabs = parse_sessionstore("sessionstore.js")
        with open("MSG", "w") as msg_file:
            msg_file.write("Backup of %d currently open tabs\n\n" % len(tabs))
            msg_file.write("\n".join(tabs))
        run(["git", "commit", "-q", "-F", "MSG"])
        os.remove("MSG")
        # Remove old history
        oldcommit = Popen(["git", "log", "-1", "@{%d month ago}" % MONTHS_TO_KEEP,
                           "--pretty=format:%H"], stdout=PIPE
                           ).communicate()[0].strip()
        run(["git", "checkout", "--orphan", "temp", oldcommit])
        run(["git", "commit", "-m", "Truncated history"])
        run(["git", "rebase", "--onto", "temp", oldcommit, "master"])
        run(["git", "branch", "-D", "temp"])
        run(["git", "gc"])

    elif action == "list":
        print "\n".join(parse_sessionstore(SESSIONSTORE_PATH))

    else:
        parser.error("Unsupported action. Either 'store' or 'list'.")



if __name__ == "__main__":
    try:
        main()
    except KeyboardInterrupt:
        print "\rExiting on user request"
